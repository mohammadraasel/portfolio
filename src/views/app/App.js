import React from 'react';
import './App.scss';
import { Routes } from '../../constants/routes'
import { Route, Switch , withRouter } from 'react-router-dom'
import HomePage from '../pages/home'
import SideBar from '../components/side-bar'
import ContactPage from '../pages/contact'
import AboutPage from '../pages/about'
import BlogPage from '../pages/blog'
import PostDetailPage from '../pages/post-detail'
import Header from '../components/header'


class App extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }
  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      window.scrollTo(0, 0)
    }
  }

  render() {
    return (
      <div className="app">
        <Header/>
        <SideBar />
        <Switch>
          <Route exact path={Routes.CONTACT_PAGE()} component={ContactPage}/>
          <Route exact path={Routes.ABOUT_PAGE()} component={AboutPage}/>
          <Route exact path={Routes.BLOG_PAGE()} component={BlogPage}/>
          <Route exact path={Routes.POST_DETAIL_PAGE(':id')} component={PostDetailPage}/>
          <Route path={Routes.HOME_PAGE()} component={HomePage}/>
        </Switch>
      </div>
    )
  }
}

export default withRouter(App);
