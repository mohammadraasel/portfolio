import React, { Component } from 'react'
import './side-bar.scss'
import {USER_PROFILE} from '../../../constants/images'
import { Link, withRouter } from 'react-router-dom'
import {
    FACEBOOK_ICON,
    INSTAGRAM_ICON,
    PINTEREST_ICON,
    YOUTUBE_ICON,
    DRIBBLE_ICON,
    BEHANCE_ICON
} from '../../../constants/images'
import { ReactSVG } from 'react-svg'
import { Routes } from '../../../constants/routes'


export class SideBar extends Component {

    openLinkWindow(url){
        window.open(url, "_blank")
    }

    render() {
        const {pathname} = this.props.location
        return (
            <div className="side-bar">
                <div className="side-bar-container">
                    <div className="profile-picture">
                        <div className="frame">
                            <img src={USER_PROFILE} alt="profile"/>
                        </div>
                    </div>

                    <div className="user-info">
                        <h3 className="heading">
                            Md Saiful Islam
                        </h3>
                        <p className="sub-heading">
                            A design lover from Bangladesh
                            A design lover from Bangladesh
                            A design lover from Bangladesh
                            
                        </p>
                    </div>

                    

                    <div className="nav">
                        <ul className="nav-items">
                            <li className="item">
                                <Link className={pathname === Routes.HOME_PAGE()? 'active' : ''} to="/">Home</Link>
                            </li>
                            <li className="item">
                                <Link className={pathname === '/blog' ? 'active' : ''}  to="/blog">Blog</Link>
                            </li>
                            <li className="item">
                                <Link className={pathname === '/about' ? 'active' : ''} to="/about">About</Link>
                            </li>
                            <li className="item">
                                <Link className={pathname === Routes.CONTACT_PAGE()? 'active' : ''} to="/contact">Contact</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="social-links">
                        <div 
                            className="link"
                            onClick={()=>this.openLinkWindow('https://www.pinterest.com/saifulislamdnr')}
                        >
                            <ReactSVG className='social-icon pn' src={PINTEREST_ICON} />
                        </div>
                        <div 
                            className="link"
                            onClick={()=>this.openLinkWindow('https://dribbble.com/Saifulislamdgnr')}
                        >
                            <ReactSVG className='social-icon dr' src={DRIBBLE_ICON} />
                        </div>
                        <div 
                            className="link"
                            onClick={()=>this.openLinkWindow('https://www.behance.net/saifulislam67')}
                        >
                            <ReactSVG className='social-icon bh' src={BEHANCE_ICON} />
                        </div>
                        <div 
                            className="link"
                            onClick={()=>this.openLinkWindow('https://www.instagram.com/saifulislamdnr')}
                        >
                            <ReactSVG className='social-icon ins' src={INSTAGRAM_ICON} />
                        </div>
                        <div 
                            className="link"
                            onClick={()=>this.openLinkWindow('https://www.facebook.com/saifulislamdnr')}
                        >
                            <ReactSVG className='social-icon fb' src={FACEBOOK_ICON} />
                        </div>
                        <div 
                            className="link"
                            onClick={()=>this.openLinkWindow('https://www.youtube.com/channel/UC-LcCMMz_tk4psAC3Nfkisw?view_as=subscriber')}
                        >
                            <ReactSVG className='social-icon yt' src={YOUTUBE_ICON} />
                        </div>
                        
                    </div>
                    <div className="footer">
                        Made with <span>❤</span> by Someone
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(SideBar)
