import React, { Component } from 'react'
import './gallery-container.scss'
import GalleryItem from '../gallery-item'
import { BUSINESS_CATEGORY, WRAN_CATEGORY, THE_COFFEE } from '../../../constants/images'

const gallery_items = {
    stationary: [
        {   
            image: BUSINESS_CATEGORY,
            label: "The Business Style"
        },
        {
            image: WRAN_CATEGORY,
            label: "The Wren"
        },
        {
            image: THE_COFFEE,
            label: "The Coffee"
        }
    ],
    flyer: [],
    bruchure: [],
    editing: []
}
export class GalleryContainer extends Component {
    render() {
        return (
            <div id='gallery' className='gallery' >
                <div className="items-container">
                    {
                        gallery_items['stationary'].map((item, index) => {
                            return (
                                <GalleryItem key={index} item={item}/>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default GalleryContainer
