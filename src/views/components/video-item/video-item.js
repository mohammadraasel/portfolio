import React, { Component } from 'react'
import { comp1 } from '../../../constants/videos'
import './video-item.scss'

export class VideoItem extends Component {
    render() {
        return (
            <div className='video-item'>
                <video className='video' controls autoPlay loop muted>
                    <source src={comp1} type="video/mp4"/>
                </video>
            </div>
        )
    }
}

export default VideoItem
