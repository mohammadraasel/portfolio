import './gallery-item.scss'
import React, { Component } from 'react'

export class GalleryItem extends Component {
    render() {
        return (
            <div className='gallery-item'>
                
                <img className='image' src={this.props.item.image} alt={this.props.item.label} />
                <div className="label">{this.props.item.label}</div>
            </div>
        )
    }
}

export default GalleryItem
