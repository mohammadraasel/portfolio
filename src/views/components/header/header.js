import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { Routes } from '../../../constants/routes'
import { MENU_ICON, CLOSE_ICON } from '../../../constants/images'
import {ReactSVG} from 'react-svg'
import './header.scss'

export class Header extends Component {
    state = {
        isSmallScreen: false,
        isMenuOpen: false
    }
    componentDidMount() {
        this.mediaQuery = window.matchMedia("(max-width: 950px)");
        this.mediaQuery.addListener(this.handleMediaQueryChange);
        this.handleMediaQueryChange(this.mediaQuery);
    }

    componentWillUnmount() {
        this.mediaQuery.removeListener(this.handleMediaQueryChange);
    }

    handleMediaQueryChange = mediaQuery => {
        if (mediaQuery.matches) {
            this.setState({isSmallScreen: true })
        } else {
            this.setState({isSmallScreen: false })
        }
    }

    toggleNav = () => {
        this.setState((prevState) => {
            return {
                isMenuOpen: !prevState.isMenuOpen
            }
        })
    }

    render() {
        const {pathname} = this.props.location
        return (
            <div className="header">
                <div className="header-container">
                    {
                        this.state.isSmallScreen? 
                            <div className='menu-container'>
                                <ReactSVG onClick={this.toggleNav} className='menu-icon' src={this.state.isMenuOpen? CLOSE_ICON : MENU_ICON}/>
                            </div> : null
                            
                    }
                    <div className={`nav ${!this.state.isMenuOpen? 'hidden-nav':''}`}>
                        <ul className="nav-items">
                            <li className="item">
                                <Link className={pathname === Routes.HOME_PAGE()? 'active' : ''} to="/">Graphics Design</Link>
                            </li>
                            
                            <li className="item">
                                <Link className={pathname === '/motion'? 'active' : ''} to="/motion">Motion Graphics</Link>
                            </li>
                            <li className="item">
                                <Link className={pathname === '/video-editing'? 'active' : ''} to="/video-editing">Video Editing</Link>
                            </li>
                            <li className="item">
                                <Link className={pathname === '/ui-ux'? 'active' : ''} to="/ui-ux">UI/UX Design</Link>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        )
    }
}

export default withRouter(Header)
