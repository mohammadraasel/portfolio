import './design-categories.scss'
import React, { Component } from 'react'
import { BUSINESS_CATEGORY, WRAN_CATEGORY } from '../../../constants/images'

const categories = [
        {value: 'stationary', label: "Brand Stationary", imageSrc:BUSINESS_CATEGORY},
        {value: 'flyer', label: "Flyer Design", imageSrc:BUSINESS_CATEGORY},
        {value: 'bruchure', label: "Bruchure Design", imageSrc: WRAN_CATEGORY },
        {value: 'catalog', label: "Catalog Design" , imageSrc:WRAN_CATEGORY},
        {value: 'poster', label: "Poster Design", imageSrc:BUSINESS_CATEGORY},
        { value: 'banner', label: "Banner Ads Design", imageSrc: BUSINESS_CATEGORY },
        {value: 'packeging', label: "Packeging Design", imageSrc:WRAN_CATEGORY},
        {value: 'invitation', label: "Invitation Card Design", imageSrc:BUSINESS_CATEGORY},
        {value: 'magazine', label: "Book/Magazine Design", imageSrc:BUSINESS_CATEGORY},
        {value: 'resume', label: "Resume Design", imageSrc:WRAN_CATEGORY},
        {value: 'calendar', label: "Calendar Design", imageSrc:BUSINESS_CATEGORY},
        { value: 'billboard', label: "Billboard Design", imageSrc: BUSINESS_CATEGORY },
        {value: 'editing', label: "Photo Editing/Retouch", imageSrc:BUSINESS_CATEGORY },
        {value: 'social', label: "Social Media Design", imageSrc: WRAN_CATEGORY },
        {value: 'tshirt', label: "T-shirt Design", imageSrc:BUSINESS_CATEGORY},
        {value: 'infographic', label: "Infographic Design", imageSrc: BUSINESS_CATEGORY },
        {value: 'cartoon', label: "Cartoon And Comics Design", imageSrc:WRAN_CATEGORY},
        { value: 'vector', label: "Vector Tracing", imageSrc: BUSINESS_CATEGORY },
        {value: 'presentation', label: "Presentation Design", imageSrc:BUSINESS_CATEGORY},
        {value: 'illustration', label: "Illustration", imageSrc:BUSINESS_CATEGORY},
    ]
   


class DesignCategories extends Component {
    render() {
        return (
            <div className='design-category'>
                <div className="branding-category">
                    {
                        categories.map(category => {
                            return (
                                <div key={category.value} className="category">
                                    <img className='background-img' src={category.imageSrc} alt={category.label}/>
                                    <div className="overlay">
                                        <div onClick={()=> this.props.onCategoryClicked(category.value, category.label)} className="btn">{category.label}</div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default DesignCategories
