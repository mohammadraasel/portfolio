import React, { Component } from 'react'
import './about-page.scss'
import { ABOUT_ME } from '../../../constants/images'

class AboutPage extends Component {
    render() {
        return (
            <div className='about-page'>
                <div className='about-page-container'>
                    <div className="image-container">
                        <img src={ABOUT_ME} alt="profile" className="image"/>
                    </div>
                    <div className="about-me">
                        <h3 className="heading">About</h3>
                        <p className="para">
                            I am a graphic designer, Video Editor, UI/UX and Motion Graphic Designer. I can create in a broad range of styles to facilitate exploratory efforts and expand your creative potential. I can partner with you to conceive, develop, produce and deploy effective solutions for your projects
                        </p>
                        <p className='para'>
                            I have (4 years' experience in 2020) These sector & communications professional. Now I focus all of my time on helping clients of my own. Whether you’re looking to new or redesign your any project just need help with, well done! You found the man for the job. From time to time I get paid to coach people so they become as awesome as I Since you landed here you are probably looking for someone who can build your new graphic design, video editing, ui/ux and motion graphic.
                        </p>
                        <p className="para">
                            My design process is simple. My Design process always starts with a pencil and paper, scribbling ideas and concepts, thinking about the problem and the solution. I approach my work using pragmatic creativity to conceive, create, and commence your project through each critical stage of development. Ensuring the result is compelling, thought-provoking, and reactive for your intended audience. my creative process is refreshingly uncomplicated.
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}

export default AboutPage
