import './post-detail-page.scss'
import React, { Component } from 'react'
import { posts } from '../blog/posts'
import { Routes } from '../../../constants/routes'
// import  marked  from 'marked'

export class PostDetailPage extends Component {
    constructor(props) {
		super(props)
		this.state = {
			postDetail: null
		}
	}

	componentDidMount() {
		window.scrollTo(0, 0)
		const post = posts.find(post=> post.id.toString() === this.props.match.params.id)
		if (post) {
			this.setState({ postDetail: post })
		} else {
			this.props.history.push(Routes.BLOG_PAGE())
		}
	}
    render() {
        const {postDetail} = this.state
        return (
            <div className='post-detail-page'>
                <div className="post-detail-page-container">
                    {
                        this.state.postDetail? 
                            <div className='content'>
                                <div className="title">{postDetail.title}</div>
                                <img src={postDetail.image} alt="post detail" className="post-image"/>
                                <div className="post-body">
                                    <p>{postDetail.body}</p>
                                </div>
                            </div> : null
                    }
                </div>
            </div>
        )
    }
}

export default PostDetailPage
