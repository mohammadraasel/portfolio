import React, { Component } from 'react'
import './contact-page.scss'

export class ContactPage extends Component {
    render() {
        return (
            <div className='contact-page'>
                <div className="contact-page-container">
                    <div className="contact-form">
                        <div className="form-control">
                            <label htmlFor="name">Your Name (required)</label>
                            <input placeholder='Name' id="name" type="text" required/>
                        </div>
                        <div className="form-control">
                            <label htmlFor="email">Your Email (required)</label>
                            <input placeholder='example@gmail.com' id="email" type="email" required/>
                        </div>
                        <div className="form-control">
                            <label htmlFor="subject">Subject</label>
                            <input placeholder='Subject' id='subject' type="text"/>
                        </div>
                        <div className="form-control">
                            <label htmlFor="message">Your Message</label>
                            <textarea placeholder='Type message here...' name="message" id="message" cols="70" rows="13"/>
                        </div>
                        <div className="button-container">
                            <div className="button">Send</div>
                        </div>
                    </div>
                    <div className="contact-details">
                        <h3 className="heading">Contact Details</h3>
                        <p className="hesitate-text">If you want to work with me don’t hesitate to contact me.<br/> I work as freelancer and a dreamer you are welcome.</p>

                        <div className="address"><strong>Address:</strong> 143, Shantibag, Shajahanpur, Dhaka - 1217</div>

                        <div className="cell"><strong>Cell:</strong> +8801866290587</div>
                        <div className="email"><strong>Email:</strong>  saifulislamdnr@gmail.com</div>

                    </div>
                </div>
            </div>
        )
    }
}

export default ContactPage
