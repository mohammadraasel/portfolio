import React, { Component } from 'react'
import './home-page.scss'
import GalleryContainer from '../../components/gallery-container'
import {withRouter} from 'react-router-dom'
import VideoItem from '../../components/video-item/video-item'

class HomePage extends Component {
    state = {
        categoryName: null,
        title: ''
    }

    componentDidMount() {
        window.scrollTo(0, 1)
    }
    
    onCategoryClicked = (category, title) => {
        this.setState({ categoryName: category, title }, () => {
            this.scrollTo('#gallery');
        })
        
    }

    scrollTo = targetId => {
		const target= document.querySelector(targetId)
		if (!target) {
			return
		}

		const offset = target.offsetTop - 120
		window.scrollTo(0, offset)
	}

    render() {
        console.log(this.props.location.pathname)
        return (
            <div className='home-page'>
                <div className="home-page-container">

                    {
                        this.props.location.pathname === '/' &&
                        <div className="graphics">
                            <GalleryContainer/>
                        </div>
                    }
                    {
                        this.props.location.pathname === '/motion' &&
                        <div className="motion">
                            <VideoItem/>
                        </div>
                    }
                </div>
            </div>
        )
    }
}

export default withRouter(HomePage)
