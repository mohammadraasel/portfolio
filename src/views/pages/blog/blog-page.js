import React, { Component } from 'react'
import './blog-page.scss'
import { posts } from './posts'
import { Routes } from '../../../constants/routes'
import { withRouter } from 'react-router-dom'

export class BlogPage extends Component {
    constructor(props){
        super(props)
        this.state = {
            posts: posts
        }
    }
    render() {
        return (
            <div className='blog-page'>
                <div className="blog-page-container">
                    <div className="posts-container">
                        {
                            this.state.posts && this.state.posts.map(post=>(
                                <div 
                                    className='post' 
                                    key={post.id}
                                    onClick={()=>this.props.history.push(Routes.POST_DETAIL_PAGE(post.id))}
                                >
                                    <div className="post-image-container">
                                        <img src={post.image} alt="post" className="post-image"/>
                                    </div>
                                    <div className="post-text-container">
                                        <h3 className="title">{post.title}</h3>
                                        <p className='sample-text'>
                                            {post.sampleText}
                                        </p>
                                        <p className='time-date'>
                                            <span>{post.createdAt}</span>
                                            <span>{post.readLength}</span>
                                        </p>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(BlogPage)
