import {
    post_1,
    THE_COFFEE
} from '../../../constants/images'

export const posts = [
    {
        id: 1,
        title: "The four basic design principles",
        createdAt:"26 Dec 2019",
        readLength: "2 min read",
        image:post_1,
        sampleText:'The basic principles of design that appear in every well designed piece of work. Although I discuss each one of these principles separately, keep in mind they are really interconnected. Rarely will you apply only...',
        body: `The basic principles of design that appear in every well designed piece of work. Although I discuss each one of these principles separately, keep in mind they are really interconnected. Rarely will you apply only one principle.
        \n\n### Contrast
        The idea behind contrast is to avoid elements on the page that are merely similar. If the
        elements (type, color, size, line thickness, shape, space, etc.) are not the same, then make them very different. Contrast is often the most important visual attraction on a page—it’s what makes a reader look at the page in the first place.
        \n\n### Repetition
        Repeat visual elements of the design throughout the piece. You can repeat colors, shapes, textures, spatial relationships, line thicknesses, fonts, sizes, graphic concepts, etc. This develops the organization and strengthens the unity.
        \n\n### Alignment
        Nothing should be placed on the page arbitrarily. Every element should have some visual connection with another element on the page. This creates a clean, sophisticated, fresh look.
        \n\n### Proximity
        Items relating to each other should be grouped close together. When several items are in close proximity to each other, they become one visual unit rather than several separate units. This helps organize information, reduces clutter, and gives the reader a clear structure.
        `
    },

    {
        id: 2,
        title: "This is a dummy post for testing",
        createdAt:"27 Dec 2019",
        readLength: "5 min read",
        image:THE_COFFEE,
        sampleText:'The basic principles of design that appear in every well designed piece of work. Although I discuss each one of these principles separately, keep in mind they are really interconnected. Rarely will you apply only...',
        body: `The basic principles of design that appear in every well designed piece of work. Although I discuss each one of these principles separately, keep in mind they are really interconnected. Rarely will you apply only one principle.Contrast The idea behind contrast is to avoid elements on the page that are merely similar. If the elements (type, color, size, line thickness, shape, space, etc.) are not the same, then make them very different. Contrast is often the most important visual attraction on a page—it’s what makes a reader look at the page in the first place.Repetition Repeat visual elements of the design throughout the piece. You can repeat colors, shapes, textures, spatial relationships, line thicknesses, fonts, sizes, graphic concepts, etc. This develops the organization and strengthens the unity.Alignment Nothing should be placed on the page arbitrarily. Every element should have some visual connection with another element on the page. This creates a clean, sophisticated, fresh look.Proximity Items relating to each other should be grouped close together. When several items are in close proximity to each other, they become one visual unit rather than several separate units. This helps organize information, reduces clutter, and gives the reader a clear structure.
        `
    }
]
