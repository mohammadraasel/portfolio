export const Routes = {
	HOME_PAGE: () => '/',	
	CONTACT_PAGE: () => '/contact',
	ABOUT_PAGE: () => '/about',
	BLOG_PAGE: () => '/blog',
	POST_DETAIL_PAGE: (id) => `/blog/posts/${id}`,

}